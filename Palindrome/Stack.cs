﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Palindrome
{
    public class Stack
    {
        private List<object> stacks = new List<object>();
        private int top = 0;

        public void Push(object item)
        {
            stacks.Add(item);
            top++;
        }

        public object Pop()
        {
            top--;
            return stacks[top];
        }

    }
}
