﻿using System;

namespace Palindrome
{
    class Program
    {
        static void Main(string[] args)
        {
            string sentence = "Never a foot too far, even.";
            checkPalindrome(sentence);
        }

        private static void checkPalindrome(string sentence)
        {
            char[] charSentence = sentence.ToCharArray();
            string currentSentence = "";
            string popSentence = "";
            Stack stack = new Stack();
            foreach (var item in charSentence)
            {
                if (char.IsLetter(item) && !char.IsWhiteSpace(item))
                {
                    stack.Push(item);
                    currentSentence += item;
                }
            }

            for (int i = 0; i < currentSentence.Length; i++)
            {
                popSentence += stack.Pop();
            }

            if (popSentence.ToLower().Equals(currentSentence.ToLower()))
            {
                Console.WriteLine($"{sentence} is a palindrome.");
            }
            else
            {
                Console.WriteLine($"{sentence} is not a palindrome.");
            }
        }
    }
}
