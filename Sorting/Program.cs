﻿using System;

namespace Sorting
{
    class Program
    {
        static void Main ( string [] args )
        {
            int [] numbers = { 50, 60, 10, 20, 30, 40 };
            Console.WriteLine ("Insertion Sort : ");
            
            for ( int i = 0; i < numbers.Length; i++ )
            {
                Console.WriteLine (InsertionSort (numbers)[i]);
            }
            Console.WriteLine ("Bubble Sort : ");
            for ( int i = 0; i < numbers.Length; i++ )
            {
                Console.WriteLine (BubbleSort (numbers)[i]);
            }

            

        }

        public static int[] InsertionSort ( int [] numbers )
        {
            int val,flag;
            for (int i = 1; i < numbers.Length; i++ )
            {
                val = numbers [i];
                flag = 0;
                for ( int j = i - 1; j >= 0 && flag != 1; )
                {
                    if ( val < numbers [j] )
                    {
                        numbers [j + 1] = numbers [j];
                        j--;
                        numbers [j + 1] = val;
                    }
                    else flag = 1;
                }
            }

            return numbers;

        }

        public static int[] BubbleSort ( int [] numbers )
        {
            int temp;
            for ( int j = 0; j <= numbers.Length - 2; j++ )
            {
                for ( int i = 0; i <= numbers.Length - 2; i++ )
                {
                    if ( numbers [i] > numbers [i + 1] )
                    {
                        temp = numbers [i + 1];
                        numbers [i + 1] = numbers [i];
                        numbers [i] = temp;
                    }
                }
            }

            return numbers;
        }
    }
}
