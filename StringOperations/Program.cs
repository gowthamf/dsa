﻿using System;

namespace StringOperations
{
    class Program
    {
        static void Main ( string [] args )
        {
            String value = "This is a Test";
            int n1 = 50, n2 = 10;
            int total = n1 + n2;

            Console.WriteLine (value.Substring (4));
            Console.WriteLine ($"Sum of {n1} and {n2} is {total}");
            var val = value.Split (" ");
            foreach ( var item in val )
            {
                Console.WriteLine (item);
            }
            
        }
    }
}
