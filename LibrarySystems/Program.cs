﻿using System;
using System.Collections.Generic;

namespace LibrarySystems
{
    class Program
    {
        static void Main ( string [] args )
        {
            Que q = new Que ();
            List<Members> members = new List<Members> ();
            members.Add (new Members { MemberId = 1, BookId = 1, IsAccepted = false });
            members.Add (new Members { MemberId = 2, BookId = 2, IsAccepted = true });
            members.Add (new Members { MemberId = 3, BookId = 3, IsAccepted = false });
            members.Add (new Members { MemberId = 4, BookId = 4, IsAccepted = false });
            members.Add (new Members { MemberId = 5, BookId = 5, IsAccepted = false });
            members.Add (new Members { MemberId = 6, BookId = 6, IsAccepted = false });
            members.Add (new Members { MemberId = 7, BookId = 7, IsAccepted = false });

            foreach ( var item in members )
            {
                q.Enqueue (item);
            }

            foreach ( var item in members )
            {
                if(item.IsAccepted)
                {
                    q.DeleteNode (item);
                }
            }
        }
    }

    class QueNode
    {
        public Members Member { get; set; }

        public QueNode Next { get; set; }

        public QueNode ( Members member )
        {
            Member = member;
            Next = null;
        }
    }

    class Que
    {
        QueNode front, rear;

        public Que ( )
        {
            front = null;
            rear = null;
        }

        public void Enqueue ( Members member )
        {
            QueNode queNode = new QueNode (member);

            if ( rear == null )
            {
                front = queNode;
                rear = queNode;
                return;
            }

            rear.Next = queNode;
            rear = queNode;
        }

        public QueNode Dequeue ( )
        {
            if ( front == null )
            {
                return null;
            }

            QueNode queNode = front;
            front.Next = front.Next;

            if ( front == null )
            {
                rear = null;
            }

            return queNode;
        }

        public void DeleteNode ( Members members )
        {
            QueNode queNode = front;
            QueNode quePrev = null;

            if ( queNode != null && queNode.Member.MemberId == members.MemberId || queNode.Next.Member.MemberId==members.MemberId )
            {
                queNode.Member = front.Next.Member;
                return;
            }

            while ( queNode != null && queNode.Member.MemberId != members.MemberId )
            {
                quePrev = queNode;
                queNode = quePrev.Next;
            }

            if ( queNode == null )
            {
                return;
            }

            quePrev.Next = queNode.Next;

        }
    }

    class Members
    {
        public int MemberId { get; set; }

        public int BookId { get; set; }

        public bool IsAccepted { get; set; }
    }
}
