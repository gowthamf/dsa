﻿using System;

namespace Searches
{
    class Program
    {
        static void Main ( string [] args )
        {
            int [] numbers = { 5, 9, 6, 10, 100, 566 };
            int findNumber = 100;
            LinearSearchNonRecursive (numbers, findNumber);
            int recursiveIndex = LinearSearchRecursive (numbers, 0, numbers.Length - 1, findNumber);
            if (recursiveIndex>=0)
            {
                Console.WriteLine ($"{findNumber} is at {recursiveIndex + 1}");
            }
            
        }

        static void LinearSearchNonRecursive(int [] numbers, int findNumber)
        {
            int position = -1;

            for ( int i = 0; i < numbers.Length; i++ )
            {
                if ( numbers [i] == findNumber )
                {
                    position = i;
                    break;
                }
            }
            if ( position >= 0 )
            {
                Console.WriteLine ($"{findNumber} is at {position + 1}");
            }
        }

        static int LinearSearchRecursive(int [] numbers, int index, int arraySize, int findNumber)
        {
            if(arraySize<1)
            {
                return -1;
            }
            if(numbers[index]==findNumber)
            {
                return index;
            }
            if(numbers[arraySize]==findNumber)
            {
                return arraySize;
            }
            return LinearSearchRecursive (numbers, index + 1, arraySize - 1, findNumber);
        }
        //
        
        

    }
}
